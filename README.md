# Dragons of Mugloar #

Dragons of Mugloar story: [Link](http://www.dragonsofmugloar.com)

Dragons of Mugloar API:[Link](https://www.dragonsofmugloar.com/doc/)

Bigbank test task. 

## Dependencies ##
   * Java 8
   * Maven

## How to run ##
1. Download, clone or fork the repository
2. Use build command:

        mvn clean package 

3. Use run command:

        mvn exec:java -D"exec.mainClass"="main.Main"