package controllers;

import http.HttpService;
import model.*;
import org.apache.log4j.Logger;

import java.io.IOException;

public class GameController {

    private final Logger logger = Logger.getLogger(GameController.class);
    Game game;

    private HttpService httpService;
    private Message[] messages;
    private Message msg;
    private Reputation rep;
    private MessageSolution ms;
    private ShoppingSolution shoppingSolution;

    private float stateReputation = 0;


    public GameController(HttpService httpService) {
        this.httpService = httpService;
    }

    public void startGame(){
        try {
            game = httpService.getGame();
            logger.info(game);
        }catch (IOException e) {
            logger.error("Failed to start a game", e);
        }
    }
    public Message[] getMessageBoard(){
        try{
            messages = httpService.getMessages(game.getGameId());
            for(Message m : messages){
                logger.info(m);
            }
            return messages;
        }catch (IOException e) {
            logger.error("Failed to fetch", e);
        }
        return null;
    }
    public void getReputation(){
        try {
            rep = httpService.getReputation(game.getGameId());
            logger.info(rep);
        }catch (IOException e) {
            logger.error("Failed to fetch rep", e);
        }
    }
    public void getMessageSolution(String adId){
        try {
            ms = httpService.getMessageResponse(game.getGameId(), adId);
            ms.setGameId(game.getGameId());
            logger.info(ms);
            logger.info("Message solution: " + ms.isSolution());
        }catch (IOException e) {
            logger.error("Failed to do message solution", e);
        }
    }

    public Message getAnyMessage(){
        Message returnMsg = null;
        for(Message m : messages) {
            if (m.isHardProbability(m)){
                returnMsg = m;
                break;
            }
        }
        return returnMsg;
    }
    public Message getBestMessage(){
        Message returnMsg = null;
        int reward = 0;
        for(Message m : messages){
            if((m.isGoodProbability(m)) && m.getReward() > reward){
                if((m.getMessage().contains("Steal"))){
                    if (stateReputation > -4.0){
                        logger.info("stateReputation" + stateReputation);
                        reward = m.getReward();
                        returnMsg = m;
                    }
                }
                else{
                    reward = m.getReward();
                    returnMsg = m;
                }
            }
        }
        return returnMsg;
    }
    public Message getMediumMessage(){
        Message returnMsg = null;
        int reward = 0;
        for(Message m : messages){
            if((m.isMediumProbability(m)) && m.getReward() > reward && !(m.getMessage().contains("Steal"))){
                reward = m.getReward();
                returnMsg = m;
            }
        }
        return returnMsg;
    }
    public boolean isGameOver(){
        if (!(ms.isSolution()) && (ms.getLives() == 0))
            return true;
        else
            return false;
    }
    public MessageSolution getMs() {
        return ms;
    }
    public void buySomething(String itemId){
        try {
            shoppingSolution = httpService.getShoppingResponse(game.getGameId(), itemId);
            //We only shop for lives for now
            if(itemId.equals("hpot")) {
                ms.setLives(ms.getLives() + 1);
                ms.setGold(shoppingSolution.getGold());
            }
            else{
                ms.setGold(shoppingSolution.getGold());
                ms.setLevel(shoppingSolution.getLevel());
            }
            logger.info(shoppingSolution);
            logger.info("info:" + ms);
        }catch (IOException e) {
            logger.error("Failed to shop", e);
        }
    }
    public void setStateReputation(float stateReputation) {
        this.stateReputation = stateReputation;
    }
    public Reputation getRep() {
        return rep;
    }
}