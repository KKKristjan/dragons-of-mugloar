package http;

import java.io.IOException;

import org.apache.http.client.fluent.Request;

public class HttpClientImpl implements HttpClient {
    public String makeGetRequest(String url) throws IOException{
        return Request.Get(url)
                .execute()
                .returnContent()
                .asString();
    }
    public String makePostRequest(String url) throws IOException {
        return Request.Post(url)
                .execute()
                .returnContent()
                .asString();
    }
}