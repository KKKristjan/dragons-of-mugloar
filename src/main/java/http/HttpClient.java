package http;

import java.io.IOException;

public interface HttpClient {
    String makePostRequest(String url) throws IOException;
    String makeGetRequest(String url) throws IOException;

}
