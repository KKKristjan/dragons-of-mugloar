package http;


import global.Global;
import model.*;

import java.io.IOException;

public class HttpService {

    private final HttpClient httpClient;
    private final Serializer serializer;

    public HttpService(HttpClientImpl httpClient, Serializer serializer) {
        this.httpClient = httpClient;
        this.serializer = serializer;
    }

    public Game getGame() throws IOException {
        String response = httpClient.makePostRequest(Global.START_GAME_URL);
        return Serializer.getFromJson(response, Game.class);
    }
    public Message[] getMessages(String gameId) throws IOException {
        String response = httpClient.makeGetRequest(Global.getMessages(gameId));
        return Serializer.getFromJson(response, Message[].class);
    }
    public Reputation getReputation(String gameId) throws IOException {
        String response = httpClient.makePostRequest(Global.getReputation(gameId));
        return Serializer.getFromJson(response, Reputation.class);
    }
    public MessageSolution getMessageResponse(String gameId, String adId) throws IOException {
        String response = httpClient.makePostRequest(Global.sendMessage(gameId, adId));
        return Serializer.getFromJson(response, MessageSolution.class);
    }
    public ShoppingSolution getShoppingResponse(String gameId, String itemId) throws IOException {
        String response = httpClient.makePostRequest(Global.buyShopItem(gameId, itemId));
        return Serializer.getFromJson(response, ShoppingSolution.class);
    }
}
