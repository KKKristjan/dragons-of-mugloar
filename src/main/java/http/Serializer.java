package http;

import com.google.gson.Gson;

public class Serializer {

    public static <T> T getFromJson(String json, Class<T> cl) {
        return new Gson().fromJson(json, cl);
    }

}
