package model;

public class Game {

    private String gameId;
    private int lives;
    private int gold;
    private int level;
    private int score;
    private int highScore;
    private int turn;

    public Game(String gameId, int lives, int gold, int level, int score, int highScore, int turn) {
        this.gameId = gameId;
        this.lives = lives;
        this.gold = gold;
        this.level = level;
        this.score = score;
        this.highScore = highScore;
        this.turn = turn;
    }

    public int getLives() {
        return lives;
    }
    public int getScore() {
        return score;
    }

    public int getGold() {
        return gold;
    }


    public String getGameId() {
        return gameId;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public int getTurn() {
        return turn;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId='" + gameId + '\'' +
                ", lives=" + lives +
                ", gold=" + gold +
                ", level=" + level +
                ", score=" + score +
                ", highScore=" + highScore +
                ", turn=" + turn +
                '}';
    }
}
