package model;

public class Message {
    String adId;
    String message;
    int reward;
    int expiresIn;
    String probability;

    public Message(String adId, String message, int reward, int expiresIn, String probability) {
        this.adId = adId;
        this.message = message;
        this.reward = reward;
        this.expiresIn = expiresIn;
        this.probability = probability;
    }

    public int getReward() {
        return reward;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public String getProbability() {
        return probability;
    }

    public boolean isGoodProbability(Message msg){
        if (msg.getProbability().equals("Walk in the park") || msg.getProbability().equals("Piece of cake")
                || msg.getProbability().equals("Quite likely") || msg.getProbability().equals("Sure thing"))
            return true;
        else
            return false;
    }
    public boolean isMediumProbability(Message msg){
        if (msg.getProbability().equals("Gamble") || msg.getProbability().equals("Risky")
                || msg.getProbability().equals("Playing with fire")|| msg.getProbability().equals("Hmmm...."))
            return true;
        else
            return false;
    }
    public boolean isHardProbability(Message msg){
        if (msg.getProbability().equals("Impossible") || msg.getProbability().equals("Suicide mission")|| msg.getProbability().equals("Rather detrimental"))
            return true;
        else
            return false;
    }
    public String getAdId() {
        return adId;
    }

    public String getMessage() {
        return message;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    @Override
    public String toString() {
        return "Message{" +
                "adId='" + adId + '\'' +
                ", message='" + message + '\'' +
                ", reward=" + reward +
                ", expiresIn=" + expiresIn +
                ", probability='" + probability + '\'' +
                '}';
    }

}
