package model;

public class ShoppingSolution extends Game {

    private boolean shoppingSuccess	;

    public ShoppingSolution(String gameId, int lives, int gold, int level, int score, int highScore, int turn, boolean success) {
        super(gameId, lives, gold, level, score, highScore, turn);
        this.shoppingSuccess	 = success;
    }
    public boolean isSuccess() {
        return shoppingSuccess	;
    }

    @Override
    public String toString() {
        return "ShoppingSolution{" +
                "success=" + shoppingSuccess	 +
                '}';
    }
}
