package model;

public class MessageSolution extends Game {

    private boolean solution;

    public MessageSolution(String gameId, int lives, int gold, int level, int score, int highScore, int turn, boolean solution) {
        super(gameId, lives, gold, level, score, highScore, turn);
        this.solution = solution;
    }
    public boolean isSolution() {
        return solution;
    }
}
