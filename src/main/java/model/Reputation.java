package model;

public class Reputation {
    float people;
    float state;

    float underworld;

    public Reputation(int people, int state, int underworld) {
        this.people = people;
        this.state = state;
        this.underworld = underworld;
    }

    public float getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Reputation{" +
                "people=" + people +
                ", state=" + state +
                ", underworld=" + underworld +
                '}';
    }
}
