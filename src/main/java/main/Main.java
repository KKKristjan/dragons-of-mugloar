package main;
import controllers.GameController;
import http.HttpClientImpl;
import http.HttpService;
import http.Serializer;
import model.Message;
import org.apache.log4j.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);
    private static HttpService httpService;
    private static Message msg;

    public static void main(String[] args) {
        httpService = new HttpService(new HttpClientImpl(), new Serializer());
        GameController gameController= new GameController(httpService);
        gameController.startGame();
        gameController.getReputation();
        gameController.getMessageBoard();
        msg = gameController.getBestMessage();
        gameController.getMessageSolution(msg.getAdId());
        while(!gameController.isGameOver()){
            if(gameController.getRep().getState() >= -4) {
                gameController.getReputation();
                gameController.setStateReputation(gameController.getRep().getState());
            }
            gameController.getMessageBoard();
            msg = gameController.getBestMessage();
            if((gameController.getMs().getLives() == 1) && (gameController.getMs().getGold() >= 50)){
                logger.info("1life only but enough cash to buy some");
                gameController.buySomething("hpot");
            }
            else if(msg == null){
                if (gameController.getMs().getGold() >= 300){
                    gameController.buySomething("rf");
                }
                else if (gameController.getMs().getGold() >= 100){
                    gameController.buySomething("cs");
                }
                else if (gameController.getMs().getGold() >= 50){
                    gameController.buySomething("hpot");
                }
                else{
                    msg = gameController.getMediumMessage();
                    if (msg == null) {
                        msg = gameController.getAnyMessage();
                        gameController.getMessageSolution(msg.getAdId());
                        logger.info("No good solutions and no money: pick any add");
                    }
                    else{
                        logger.info("Chosen medium: " + msg);
                        gameController.getMessageSolution(msg.getAdId());
                    }
                }
            }
            else {
                logger.info("Chosen: " + msg);
                gameController.getMessageSolution(msg.getAdId());
            }
        }
        logger.info("Game is over, you got:" + gameController.getMs().getScore()+" points");
        logger.info("MS " + gameController.getMs());
    }
}