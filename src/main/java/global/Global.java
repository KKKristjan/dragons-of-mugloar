package global;

public class Global {
    public static final String START_GAME_URL = "https://dragonsofmugloar.com/api/v2/game/start";

    public static String getMessages(String gameId){
        return "https://dragonsofmugloar.com/api/v2/" + gameId + "/messages";
    }
    public static String getReputation(String gameId){
        return "https://dragonsofmugloar.com/api/v2/" + gameId + "/investigate/reputation";
    }
    public static String getShopItems(String gameId){
        return "https://dragonsofmugloar.com/api/v2/" + gameId + "/shop";
    }
    public static String buyShopItem(String gameId, String itemId){
        return "https://dragonsofmugloar.com/api/v2/"+gameId+"/shop/buy/"+itemId;
    }
    public static String sendMessage(String gameId, String adId){
        return "https://dragonsofmugloar.com/api/v2/"+gameId+"/solve/"+adId;
    }

}
