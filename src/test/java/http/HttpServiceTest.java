package http;

import controllers.GameController;
import model.Message;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class HttpServiceTest {
    private HttpService service;
    GameController gameController;

    @Before
    public void setUp() {
        HttpClient client = new HttpClientImpl();
        Serializer serializer = new Serializer();
        service = new HttpService(new HttpClientImpl(), new Serializer());
        gameController= new GameController(service);
    }
    @Test
    public void startGameTest() throws Exception {
        gameController.startGame();
        assertNotNull(gameController);
    }

    @Test
    public void getMessageBoardTest() throws Exception {
        gameController.startGame();
        Message[] messages = gameController.getMessageBoard();
        assertNotNull(messages);
    }
}
